package com.sfatandrei;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@EnableAutoConfiguration
public class PubSubHubBubSubscriberController {

    private static RestTemplate restTemplate = new RestTemplate();

    static {
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
    }


    @RequestMapping(value = "/subscribe")
    @ResponseBody
    public String subscribe() {
        HttpEntity<MultiValueMap<String, String>> requestEntity =
                prepareSubscribePostRequest();

        ResponseEntity<String> responseEntity =
                restTemplate.exchange("https://pubsubhubbub.superfeedr.com", HttpMethod.POST, requestEntity, String.class);
        return responseEntity.getBody();
    }

    private HttpEntity<MultiValueMap<String, String>> prepareSubscribePostRequest() {
        String topicUrl = "https://superfeedr-blog-feed.herokuapp.com/";

        MultiValueMap<String, String> postParameters = new LinkedMultiValueMap<String, String>();
        postParameters.put("hub.mode", Collections.singletonList("subscribe"));
        postParameters.put("hub.callback", Collections.singletonList("http://push-pub.appspot.com/webhook"));
        postParameters.put("hub.topic", Collections.singletonList(topicUrl));
        postParameters.put("hub.verify", Collections.singletonList("sync"));


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/x-www-form-urlencoded");
        return new HttpEntity<MultiValueMap<String, String>>(postParameters, httpHeaders);
    }

    @RequestMapping(value = "/callback", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
    @ResponseBody
    public String callback() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        SpringApplication.run(PubSubHubBubSubscriberController.class, args);
    }
}
